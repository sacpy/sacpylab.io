import pandas as pd # using pd as an alias when using pandas objects
import os

## the equal sign is used for object assignment

## Note: Alt + Enter executes the line of code where the cursor is at

# in Python the first index starts at 0
# name of path to file
path = "F:\\Data_Science_Books\\Presentations\\Python Data Frame vs R Dataframe 2017-08-10\\pydata-book-master\\ch08"
# get current default working directory
file_name = "tips.csv"
#
os.getcwd()
# change the working directory
os.chdir(path)
# list the files in the working directory
ls

# the 2 periods goes to the parent folder and then to the data folder
#pandas.read_csv('../data/gapminder.tsv', sep = '\t')
# read the tips.csv file
tips = pd.read_csv(file_name)
# get the metadata of the dataframe
tips.dtypes # you can see the datatype of each column
# what type of object tips is?
type(tips)
# get a few records of the csv data from memory
tips.head()
# get a few records starting from the bottom
tips.tail()
# "shape" method gives you the dimensions of the data frame # of columns & rows
# number of records from dataframe. .shape attribute of a dataframe 
tips.shape[0] # you can use   len(tips) 

# number of columns in dataframe
tips.shape[1]
# column names in dataframe
tips.columns
# add a "column" to a dataframe
tips['data_source'] =file_name# file_name is the variable containing the name of the file
# another example
tips['tip_pct'] = tips['tip'] / tips['total_bill']
# select a column from dataframe
tips['total_bill']
# select multiple columsn from dataframe. use brackets inside tips[.....]
tips[['total_bill', 'tip', 'sex', 'day']]
# cross tab agregation in dataframe: use pandas crosstab function. i.e. pd.cross()
pd.crosstab(tips['day'], tips['size']) # it perform counts
#
# summarize data (grouping data). it aggregate the columns with numeric values
# based on the column datatype
# it aggregates on one column..time column
#  tips.dtypes #
tips.groupby(tips['time']).sum()
#
# aggregate on 2 columns:
# 2 step process
# step 1
grouped = tips.groupby(['sex', 'smoker'])
# step 2
grouped_pct = grouped['tip_pct']
# step 3
grouped_pct.agg('mean')
###
pd.pivot_table(tips, index = ['sex', 'smoker'])
# or. here below you specify explicitly which columns to aggregate on
pd.pivot_table(tips, values = ['size', 'tip', 'tip_pct', 'total_bill'], index= ['sex', 'smoker'])
# another example
# aggregating on multiple brackets
pd.pivot_table(tips, values = ['size', 'tip', 'tip_pct', 'total_bill'], index= ['sex', 'day'],columns = 'smoker', margins = True)
#
# filter dataframe by column value
tips[tips['sex'] == 'Female']

# fiter dataframe by column value. This time I look for the lowercase
# apply first the str.lower() function to the dataframe column before comparing with the lowercase value, "female"
tips[tips['sex'].str.lower() == 'female']
# filter dataframe on multiple column: use comma to enter criteria. Use parenthesis and "&" to add more criteria
tips[(tips['sex'] == 'Female') & (tips['smoker'] =='Yes')]

# filter by index.
# let's say you are looping through the dataframe
# Python index always starts at 0
tips.ix[6]
##
